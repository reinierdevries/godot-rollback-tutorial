extends Node2D

@onready var connection_panel: PanelContainer = $CanvasLayer/ConnectionPanel
@onready var host_field: LineEdit = $CanvasLayer/ConnectionPanel/GridContainer/HostField
@onready var port_field: LineEdit = $CanvasLayer/ConnectionPanel/GridContainer/PortField
@onready var message_label: Label = $CanvasLayer/MessageLabel
@onready var sync_lost_label: Label = $CanvasLayer/SyncLostLabel

func _ready() -> void:
	# NOTE: 2, 9:06 signal connecting is semi-typed now, as in: it doesn't check the actual
	# signature on the connecting method. Next to that, we're accessing the the high level 
	# multiplayer API directly.
	multiplayer.peer_connected.connect(_on_network_peer_connected)
	multiplayer.peer_disconnected.connect(_on_network_peer_disconnected)
	multiplayer.server_disconnected.connect(_on_server_disconnected)
	SyncManager.sync_started.connect(_on_sync_manager_sync_started)
	SyncManager.sync_stopped.connect(_on_sync_manager_sync_stopped)
	SyncManager.sync_lost.connect(_on_sync_manager_sync_lost)
	SyncManager.sync_regained.connect(_on_sync_manager_sync_regained)
	SyncManager.sync_error.connect(_on_sync_manager_sync_error)

func _on_server_button_pressed() -> void:
	# NOTE: 2, 7:51: NetworkedMultiplayerENet is now called ENetMultiplayerPeer.
	var peer := ENetMultiplayerPeer.new()
	peer.create_server(int(port_field.text), 1)
	# NOTE: I suppose this should be `multiplayer` instead of `get_tree().network_peer`?
	multiplayer.multiplayer_peer = peer
	
	connection_panel.visible = false
	message_label.text = "Listening..."

func _on_client_button_pressed() -> void:
	# NOTE: 2, 8:08: NetworkedMultiplayerENet is now called ENetMultiplayerPeer.
	var peer := ENetMultiplayerPeer.new()
	peer.create_client(host_field.text, int(port_field.text))
	# NOTE: I suppose this should be `multiplayer` instead of `get_tree().network_peer`?
	multiplayer.multiplayer_peer = peer
	
	connection_panel.visible = false
	message_label.text = "Connecting..."

func _on_network_peer_connected(peer_id: int) -> void:
	message_label.text = "Connected!"
	SyncManager.add_peer(peer_id)

	# NOTE: 2, 22:26, these methods have been renamed.
	$ServerPlayer.set_multiplayer_authority(1)
	if multiplayer.is_server():
		$ClientPlayer.set_multiplayer_authority(peer_id)
	else:
		$ClientPlayer.set_multiplayer_authority(multiplayer.get_unique_id())
	
	if multiplayer.is_server():
		message_label.text = "Starting..."
		
		# Give a little time to get ping data.
		# NOTE: 2, 11:46: `yield` is deprecated, it's `await` now.
		# NOTE: Isn't this properly awaitable? This feels like a hack. It's probably required for
		# gaining ping data but shouldn't this be a signal on the SyncManager instead?
		await get_tree().create_timer(2.0).timeout
		SyncManager.start()

func _on_network_peer_disconnected(peer_id: int) -> void:
	message_label.text = "Disconnected"
	SyncManager.remove_peer(peer_id)

func _on_server_disconnected() -> void:
	_on_network_peer_disconnected(1)

func _on_reset_button_pressed() -> void:
	SyncManager.stop()
	SyncManager.clear_peers()
	# NOTE: 2, 12:52: This is different in Godot 4.
	var peer := multiplayer.multiplayer_peer
	if peer:
		# NOTE: 2, 12:52: This is different in Godot 4.	
		peer.close()
	get_tree().reload_current_scene()

func _on_sync_manager_sync_started() -> void:
	message_label.text = "Started!"

func _on_sync_manager_sync_stopped() -> void:
	pass

func _on_sync_manager_sync_lost() -> void:
	sync_lost_label.visible = true
	
func _on_sync_manager_sync_regained() -> void:
	sync_lost_label.visible = false

func _on_sync_manager_sync_error(msg: String) -> void:
	message_label.text = "Fatal sync error: " + msg
	sync_lost_label.visible = false

	# NOTE: 2, 15:54: This is different in Godot 4.
	var peer := multiplayer.multiplayer_peer
	if peer:
		# NOTE: 2, 15:54: This is different in Godot 4.	
		peer.close()
	SyncManager.clear_peers()
